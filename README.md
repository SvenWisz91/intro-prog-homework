Relationships of Quantopian and Zipline?

Zipline is a trading algorithm that is used, and continuously updated by Quantopian. It allows for life-trading, as well as back-testing of trades and trading strategies.


What does set_slippage function do?

It enables price manipulations by our trades on historical data when back testing. So that the trades that we execute drive the stock price up or down, which would be the case if we would have bought large amounts of the market of one stock. 
Also it limits the amount of stocks to be traded, to the usual market volume cap or the maximum volume available.
Slippage can have various additional limits or arguments, such as the magnitude to which your orders will affect prices in basis points, or you might put a volume limit. 

When using Fixed Slippage, the orders you place do not affect the price of the stocks


What does the handle_data function do?

It calls for each event the context (every minute), which is to be set and includes various variables. The context has to be defined in the initialize() function Further it calls data which is typically the data concerning the stock, e.g. OHLC (Open, High, Low, Close)

But you may also use trading start, which only updates & executes the trade once a day, at the beginning of the trading day. 


Is the _test_arg function part of the Zipline framework?

No, it is not 


Other variables


Context

Is a shell where you store the variables you will recall in each iteration


Data

Is the data you want to acces OHLC (open, high, low, close) of the stock you are observing/using in your trading bot


Other functions


Initialize()

Called always in the beginning of a back testing task
You can call what ever bookkeeping you would like to test (context) 
Calles the context (determined set of variables)
What does “pass” mean?


Ingest

Ingesting takes a data bundle (set default by Quantipian)


Order()

Takes two arguments (1. Security, 2. Quantity to buy) 
Those are then bought in each iteration


Record()

Records the progress / trades or actions undertaken by the bot in each iteration to document it